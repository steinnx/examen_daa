package com.example.duoc.jorgereyes_pruebafinal.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DUOC on 08-07-2017.
 */

public class Cancha {
    @SerializedName("id")
    private int id;
    @SerializedName("nombre_recinto")
    private String nombre_reciento;
    @SerializedName("direccion")
    private String direccion;
    @SerializedName("tipo_cancha")
    private String tipo_cancha;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("cantidad_canchas")
    private int cantidad_cancha;
    @SerializedName("telefono")
    private String telefono;
    @SerializedName("url_sitio")
    private String url_sitio;
    @SerializedName("url_avatar")
    private String url_avatar;
    @SerializedName("url_banner")
    private String url_banner;

    public Cancha() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_reciento() {
        return nombre_reciento;
    }

    public void setNombre_reciento(String nombre_reciento) {
        this.nombre_reciento = nombre_reciento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipo_cancha() {
        return tipo_cancha;
    }

    public void setTipo_cancha(String tipo_cancha) {
        this.tipo_cancha = tipo_cancha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad_cancha() {
        return cantidad_cancha;
    }

    public void setCantidad_cancha(int cantidad_cancha) {
        this.cantidad_cancha = cantidad_cancha;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrl_sitio() {
        return url_sitio;
    }

    public void setUrl_sitio(String url_sitio) {
        this.url_sitio = url_sitio;
    }

    public String getUrl_avatar() {
        return url_avatar;
    }

    public void setUrl_avatar(String url_avatar) {
        this.url_avatar = url_avatar;
    }

    public String getUrl_banner() {
        return url_banner;
    }

    public void setUrl_banner(String url_banner) {
        this.url_banner = url_banner;
    }
}
