package com.example.duoc.jorgereyes_pruebafinal.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by DUOC on 08-07-2017.
 */

public interface ApiCancha_Puente_Interface {

    @GET("bins/1487av")
    Call<CanchaResponse> getListadoCanchas();
}
