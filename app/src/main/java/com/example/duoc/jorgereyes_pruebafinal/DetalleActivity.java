package com.example.duoc.jorgereyes_pruebafinal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar2;
    private TextView tv_detalle_nombre,tv_detalle_descripcion;
    private ImageView iv_detalle_banner;
    private Button btn_sitio,btn_llamar;
    String web,contacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        Bundle extras = getIntent().getExtras();
        String foto= extras.getString("foto");
        String nombre= extras.getString("nombre");
        String descripcion= extras.getString("descripcion");
        web= extras.getString("web");
        contacto= extras.getString("contacto");

        toolbar2= (Toolbar) findViewById(R.id.toolbar_del_detalle);
        tv_detalle_nombre = (TextView) findViewById(R.id.tv_detalle_nombre_reciento);
        tv_detalle_descripcion= (TextView) findViewById(R.id.tv_detalle_descripcion);
        iv_detalle_banner = (ImageView) findViewById(R.id.iv_detalle_banner);
        btn_sitio = (Button) findViewById(R.id.btn_sitio_web);
        btn_llamar= (Button) findViewById(R.id.btn_llamar_contacto);

        toolbar2.setTitle("Jorge Reyes");
        setSupportActionBar(toolbar2);

        Picasso.with(this).load(foto).into(iv_detalle_banner);
        tv_detalle_nombre.setText(nombre);
        tv_detalle_descripcion.setText(descripcion);

        btn_sitio.setOnClickListener(this);
        btn_llamar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId()==btn_sitio.getId()){
            Uri uri = Uri.parse(web);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }else if (v.getId()==btn_llamar.getId()){
            Intent intent2 = new Intent(android.content.Intent.ACTION_DIAL,
                    Uri.parse("tel:"+contacto));
            startActivity(intent2);
        }
    }
}
