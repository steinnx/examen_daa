package com.example.duoc.jorgereyes_pruebafinal.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.http.GET;

/**
 * Created by DUOC on 08-07-2017.
 */

public class CanchaResponse {
    @SerializedName("canchas")
    List<Cancha> getCanchas;

    public CanchaResponse(List<Cancha> getCanchas) {
        this.getCanchas = getCanchas;
    }

    public List<Cancha> getGetCanchas() {
        return getCanchas;
    }

    public void setGetCanchas(List<Cancha> getCanchas) {
        this.getCanchas = getCanchas;
    }
}
