package com.example.duoc.jorgereyes_pruebafinal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.duoc.jorgereyes_pruebafinal.modelo.CanchaAdapter;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.ApiCancha_Puente_Interface;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.Cancha;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.CanchaResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContainerActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private CanchaAdapter canchaAdapter;
    private RecyclerView recyclerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        toolbar = (Toolbar) findViewById(R.id.toolbar_del_container);
        toolbar.setTitle("Jorge Reyes");
        setSupportActionBar(toolbar);

        canchaAdapter = new CanchaAdapter(this);
        recyclerView = (RecyclerView) findViewById(R.id.rv_listarCanchas);
        recyclerView.setAdapter(canchaAdapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);

        retrofit= new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        obtenerDatos();
    }

    private void obtenerDatos() {
        ApiCancha_Puente_Interface anInterface = retrofit.create(ApiCancha_Puente_Interface.class);
        Call<CanchaResponse> seriesListCall = anInterface.getListadoCanchas();
        seriesListCall.enqueue(new Callback<CanchaResponse>() {
            @Override
            public void onResponse(Call<CanchaResponse> call, Response<CanchaResponse> response) {
                if (response.isSuccessful()){
                    CanchaResponse canchaResponse = response.body();
                    List<Cancha> series = canchaResponse.getGetCanchas();
                    canchaAdapter.adiccionarLista(series);
                }else {
                    Log.e("ERRORRRRR!!!!!!!!!!!!!!","onResponeseeeeeeeeeeee: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<CanchaResponse> call, Throwable t) {
                Log.e("ERRORRRRR!!!!!!!!!!!!!!","onFailureeeeeeeeeee: "+t.getMessage());
            }
        });
    }
}
