package com.example.duoc.jorgereyes_pruebafinal.modelo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duoc.jorgereyes_pruebafinal.DetalleActivity;
import com.example.duoc.jorgereyes_pruebafinal.R;
import com.example.duoc.jorgereyes_pruebafinal.retrofit.Cancha;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DUOC on 08-07-2017.
 */

public class CanchaAdapter extends RecyclerView.Adapter<CanchaAdapter.CanchaViewHolder> {

    private ArrayList<Cancha> dataSource;
    private Context context;

    public CanchaAdapter(Context context) {
        this.context = context;
        dataSource= new ArrayList<>();
    }

    @Override
    public CanchaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_canchas,parent,false);
        return new CanchaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CanchaViewHolder holder, int position) {
        final Cancha cancha = dataSource.get(position);
        holder.tv_nombre_reciento.setText(cancha.getNombre_reciento());
        holder.tv_cantidad_canchas.setText("Cantidad de Canchas: "+cancha.getCantidad_cancha());
        holder.tv_direccion.setText(cancha.getDireccion());
        holder.tv_tipo_canchas.setText("Tipo de Cancha: "+cancha.getTipo_cancha());
        Picasso.with(context).load(cancha.getUrl_avatar()).into(holder.iv_foto_avatar);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetalleActivity.class);
                i.putExtra("foto",cancha.getUrl_banner());
                i.putExtra("nombre",cancha.getNombre_reciento());
                i.putExtra("descripcion",cancha.getDescripcion());
                i.putExtra("web",cancha.getUrl_sitio());
                i.putExtra("contacto",cancha.getTelefono());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public void adiccionarLista(List<Cancha> series) {
        dataSource.addAll(series);
        notifyDataSetChanged();
    }

    public class CanchaViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_nombre_reciento,tv_cantidad_canchas,tv_tipo_canchas,tv_direccion;
        private ImageView iv_foto_avatar;

        public CanchaViewHolder(View itemView) {
            super(itemView);
            tv_nombre_reciento = (TextView) itemView.findViewById(R.id.tv_item_nombre_reciento);
            tv_cantidad_canchas = (TextView) itemView.findViewById(R.id.tv_item_cantidad_canchas);
            tv_tipo_canchas = (TextView) itemView.findViewById(R.id.tv_item_tipo_cancha);
            tv_direccion = (TextView) itemView.findViewById(R.id.tv_item_direccion);
            iv_foto_avatar = (ImageView) itemView.findViewById(R.id.iv_item_canchas);
        }
    }
}
